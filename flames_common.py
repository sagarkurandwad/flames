from cffi import FFI

ffibuilder = FFI()
ffibuilder.cdef("""
    typedef struct {
        float x;
        float y;
    } Vec2;

    typedef struct {
      Vec2 pos;
      Vec2 dest;
      Vec2 vel;
    } MoverInfo;

    void update_mover(MoverInfo*, float, float);
""")

psim = ffibuilder.dlopen("./libpsim.so")


def update_mover(mover, proto, dt, speed):
    mover.dest.x = proto.destination.x
    mover.dest.y = proto.destination.y
    psim.update_mover(mover, dt, speed)
    proto.position.x = mover.pos.x
    proto.position.y = mover.pos.y


def advance_mover(mover, dt, speed):
    psim.update_mover(mover, dt, speed)


def set_mover_from_proto(mover, proto):
    mover.pos.x = proto.position.x
    mover.pos.y = proto.position.y
    mover.dest.x = proto.destination.x
    mover.dest.y = proto.destination.y


def create_mover():
    result = ffibuilder.new("MoverInfo*")

    result.pos.x = 0
    result.pos.y = 0
    result.dest.x = 0
    result.dest.y = 0
    result.vel.x = 0
    result.vel.y = 0

    return result