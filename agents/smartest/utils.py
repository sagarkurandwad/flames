import numpy as np
from data_pb2 import BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, DOUSED
import math
from math import floor
import random

GO_N = 0
GO_E = 1
GO_S = 2
GO_W = 3
GO_NE = 4
GO_SE = 5
GO_SW = 6
GO_NW = 7

UP = -1
DOWN = 1
RIGHT = +1
LEFT = -1
STAY = 0

EMAT=0
SMAT=1
WMAT=2
NMAT=3
SEMAT=4
SWMAT=5
NWMAT=6
NEMAT=7


direction = {EMAT:[RIGHT,UP],
            SMAT:[RIGHT,DOWN],
            WMAT:[LEFT,DOWN],
            NMAT:[LEFT,UP],
            SEMAT:[RIGHT,UP],
            SWMAT:[RIGHT,DOWN],
            NWMAT:[LEFT,DOWN],
            NEMAT:[LEFT,UP]}


"""
# straight up square
direction = {EMAT:[STAY,UP],
            SMAT:[RIGHT,STAY],
            WMAT:[STAY,DOWN],
            NMAT:[LEFT,STAY],
            SEMAT:[RIGHT,UP],
            SWMAT:[RIGHT,DOWN],
            NWMAT:[LEFT,DOWN],
            NEMAT:[LEFT,UP]}


#direction = [GO_N,GO_E,GO_S,GO_W,GO_NE,GO_SE,GO_SW,GO_NW]
"""

def action_to_dir(action):
  return direction[action]

def get_direction(data):
  
  # these are 5x5s
  n_mat = np.sum(data[:5,2:-2])
  s_mat = np.sum(data[4:,2:-2])
  w_mat = np.sum(data[2:-2,:5])
  e_mat = np.sum(data[2:-2,4:])
  sw_mat = np.sum(data[4:,0:5])
  nw_mat = np.sum(data[:5,:5])
  ne_mat = np.sum(data[:5,4:])
  se_mat = np.sum(data[4:,4:])
  
  
  # these are 3x3s
  """
  n_mat = np.sum(data[:3,3:6])
  s_mat = np.sum(data[-3:,3:6])
  w_mat = np.sum(data[3:6,:3])
  e_mat = np.sum(data[3:6,-3:])
  sw_mat = np.sum(data[-3:,:3])
  nw_mat = np.sum(data[:3,:3])
  ne_mat = np.sum(data[:3,-3:])
  se_mat = np.sum(data[-3:,-3:])
  """
  powers = [e_mat,s_mat,w_mat,n_mat,se_mat,sw_mat,nw_mat,ne_mat]

  return direction[np.argmax(powers)]

def get_fire_matrices(obs_cells,pos_x,pos_y):
  # embed obs_cells in larger matrix
  pos_x = floor(pos_x)
  pos_y = floor(pos_y)
  pos_x = max(0, min(pos_x, 71))
  pos_y = max(0, min(pos_y, 127))

  #print('TTTTTT2',pos_x,pos_y)

  orig_matrix = np.full((72,128,3),255,np.uint8)
  cell_matrix = np.zeros((72,128),np.uint8)
  for i in range(0,72):
      for j in range(0,128):
          cell_index = j * 72 + i
          #print('TTTTTTT3',cell_index)

          #print('TTTTTTT4',obs_cells[cell_index])
          if obs_cells[cell_index] == ON_FIRE:
            cell_matrix[i][j] = 1
            orig_matrix[i][j] = (0,0,255) # red
            
  large_cell_matrix = np.zeros((80,136),np.uint8)
  large_cell_matrix[4:-4,4:-4] = cell_matrix
  big_matrix = np.full((80,136,3),255,np.uint8)
  big_matrix[4:-4,4:-4] = orig_matrix

  # get 7x7 matrix
  calc_matrix = np.zeros((9,9), np.uint8)
  pos_x += 4
  pos_y += 4
  calc_matrix = large_cell_matrix[pos_x-4:pos_x+5,pos_y-4:pos_y+5]
  fire_count = np.count_nonzero(np.array(calc_matrix.flatten('F')) == 1)


  # get version to display
  little_matrix_rgb = np.full((9,9,3),255,np.uint8)
  little_matrix_rgb = big_matrix[pos_x-4:pos_x+5,pos_y-4:pos_y+5]

  fire_count = np.count_nonzero(np.array(calc_matrix.flatten('F')) == 1)

  return little_matrix_rgb,calc_matrix,fire_count


def my_random(pos_x=36,pos_y=64):
  #pos_x = random.randint(0,71) 
  #pos_y = random.randint(0,127)  

  
  direction = random.randint(0,3)
  if direction == GO_N:
    pos_y = 0
    pos_x = random.randint(0,71)
  elif direction == GO_E:
    pos_x = 71
    pos_y = random.randint(0,127)
  elif direction == GO_S:
    pos_y = 127
    pos_x = random.randint(0,71)
  elif direction == GO_W:
    pos_x = 0
    pos_y = random.randint(0,127)

  return pos_x,pos_y