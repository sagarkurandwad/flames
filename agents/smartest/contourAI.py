import cog_settings
from data_pb2 import Ai_DroneAction, BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, DOUSED

from cogment import Agent, GrpcServer
import random
import math
from math import floor
import numpy as np
import cv2
from agents.smartest.utils import get_direction, get_fire_matrices, my_random,action_to_dir
from agents.smartest.viewer import Viewer
import pickle
import sys


from agents.smartest.DQN import DQNSolver


DECISION_RANGE = 10.0
DRONE_RADIUS = 5

ACTION_SPACE = 8
OBS_SPACE = (9,9,1,)
OBS_SPACE2 = (-1,9,9,1)

MODEL_FILE_NAME = './agents/random/contourAI_model_weights.h5'


dqn_solver = DQNSolver(OBS_SPACE,ACTION_SPACE)
dqn_solver.load(MODEL_FILE_NAME)

class Ai_Drone(Agent):
    VERSIONS = {"ai_drone": "1.0.0"}
    actor_class = cog_settings.actor_classes.ai_drone

    def __init__(self, trial, actor):
        super().__init__(trial, actor)
        #self.x = 36
        #self.y = 64
        self.x,self.y = my_random()
        self.viewer = Viewer()
        self.images = []
        self.myactions = []
        self.count = 1
        self.reset_state = True
        self.fire_loop = False
        print("DQ MEM Size:",len(dqn_solver.memory))
        print('trial:',id(self),'drone:',self.id_in_class)

    def decide(self, observation):
        self.count += 1

        action = Ai_DroneAction()


        my_info = observation.drones[self.id_in_class]

        if True:  #self.id_in_class == 0:
        
            '''
            print('count:',self.count)
            if self.count == 900:
                print('Got to write images')
                with open('images.pkl','wb') as f:
                    pickle.dump(self.images, f)
                    #f.flush()
                    f.close()
                with open('actions.pkl','wb') as f:
                    pickle.dump(self.myactions, f)
                    f.close()
            '''


            if self.reset_state:
                # following is the get the first observation state
                my_matrix,self.calc_matrix,fire_count = get_fire_matrices(observation.cells,my_info.position.x,my_info.position.y)
                if fire_count != 0:
                    self.fire_loop = True
                #self.viewer.show_(my_matrix,'image')
                self.images.append(self.calc_matrix)
                self.calc_matrix = np.reshape(self.calc_matrix, OBS_SPACE2)
                self.action, self.qvals = dqn_solver.act(self.calc_matrix)  # this is just a dummy action
                self.myactions.append('NO FIRE NEW DIR')
                self.x,self.y = my_random()
                self.reset_state = False


            else:   # not reset_state

                my_matrix, self.calc_matrix_next, fire_count = get_fire_matrices(observation.cells,my_info.position.x,my_info.position.y)

                if self.id_in_class == 0:
                    self.viewer.show_(my_matrix,'image')


                if fire_count != 0:   # if there's fire
                    if self.fire_loop == True:
                        reward = 1.0
                    else:
                        reward = -1.0
                    self.images.append(self.calc_matrix_next)
                    self.calc_matrix_next = np.reshape(self.calc_matrix_next, OBS_SPACE2)
                    dqn_solver.remember(self.calc_matrix, self.action, reward, self.calc_matrix_next, observation.done, self.qvals)
                    self.calc_matrix = self.calc_matrix_next

                    if not observation.done:
                        dqn_solver.experience_replay()
                        self.action, self.qvals = dqn_solver.act(self.calc_matrix)
                        new_direction = action_to_dir(self.action)
                        self.myactions.append(new_direction)
                        self.x = floor(my_info.position.x) + new_direction[0]
                        self.y = floor(my_info.position.y) + new_direction[1]
                        self.x = max(0, min(self.x, 71))
                        self.y = max(0, min(self.y, 127))
                    self.fire_loop = True
                else:
                    if abs(my_info.position.x - my_info.destination.x) < 1 and abs(my_info.position.y - my_info.destination.y) < 1:
                        #print('NO FIRE NEW DIRECTION')
                        self.myactions.append('NO FIRE NEW DIR')
                        self.x,self.y = my_random()
                    else:
                        #print('NO FIRE CONTINUE')
                        self.myactions.append('NO FIRE CONT')
                        self.x = my_info.destination.x
                        self.y = my_info.destination.y
                    if self.fire_loop == True:  # means previous step had fire and current doesn't
                        reward = -1.0
                        self.images.append(self.calc_matrix_next)
                        self.calc_matrix_next = np.reshape(self.calc_matrix_next, OBS_SPACE2)
                        dqn_solver.remember(self.calc_matrix, self.action, reward, self.calc_matrix_next, observation.done, self.qvals)
                        self.calc_matrix = self.calc_matrix_next
                        if not observation.done:
                            dqn_solver.experience_replay()
                            self.action, self.qvals = dqn_solver.act(self.calc_matrix)
                            new_direction = action_to_dir(self.action)
                            self.myactions.append(new_direction)
                    self.fire_loop = False


            #print(f"{id(self)}, x: {self.x}, y: {self.y}")

        else:
            # for other drones
            self.x = my_info.position.x
            self.y = my_info.position.y

        # do action
        p = action.path.add()
        p.x = self.x
        p.y = self.y


        return action

    def reward(self, reward):
        pass

    def end(self):
        if self.id_in_class == 0:
            print('**************************************************************')
            print('Self count:',self.count,'Exploration:',dqn_solver.exploration_rate)

            dqn_solver.save(MODEL_FILE_NAME)
            #score = dqn_solver.get_score()
            #print('Test loss:', score[0])
            #print('Test accuracy:', score[1])               


    

if __name__ == '__main__':
    server = GrpcServer(Ai_Drone, cog_settings)
    server.serve()
