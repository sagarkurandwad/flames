import numpy as np
from data_pb2 import BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, DOUSED
import math
from math import floor
import random
from agents.fake_human.viewer import Viewer
#from scipy import ndimage as ndi
#from skimage.feature import peak_local_max
#from skimage import data, img_as_float
#from skimage import feature
import scipy.ndimage as ndimage
import scipy.ndimage.filters as filters
from scipy.signal import convolve2d
from agents.fake_human.utils import my_random
import math

def get_gradient_matrix(obs_cells,color_matrix,mono_matrix,count):

  for i in range(0,72):
      for j in range(0,128):
          cell_index = j * 72 + i
          #print('TTTTTTT3',cell_index)

          #print('TTTTTTT4',obs_cells[cell_index])
          if obs_cells[cell_index] == ON_FIRE:
            mono_matrix[i][j] = np.uint16(count)
            color_matrix[i][j] = (0,count,255) # red
  return color_matrix, mono_matrix


def find_maxima(mono_matrix,count):
  neighborhood_size = 25
  threshold = count-1
  myones = np.ones((3,3))
  #data = convolve2d(mono_matrix, myones, mode='same')
  data = mono_matrix
  data_max = filters.maximum_filter(data, neighborhood_size)
  maxima = (data == data_max)
  data_min = filters.minimum_filter(data, neighborhood_size)
  diff = ((data_max - data_min) > threshold)   # modify theshold
  maxima[diff == 0] = 0
  labeled, num_objects = ndimage.label(maxima)
  xy = np.array(ndimage.center_of_mass(data, labeled, range(1, num_objects+1)))
  # note that xy[:,1] is x coordinate array - this maybe should be switched around
  #           xy[:,0] is y coordinate array
  print("Len:", len(xy))
  print("XY:",xy)
  if len(xy) != 0:
    return xy[:,1][0],xy[:,0][0]
  else:
    return my_random()

def get_next_move(mono_matrix,count,xpos,ypos):
  if np.max(mono_matrix) == count:
    max_list = np.where(mono_matrix==count)  # max_list
    min_dist_list = []
    for i in range(len(max_list[0])):
      abs_value = math.sqrt((xpos - max_list[0][i])**2 + (ypos - max_list[1][i])**2)
      min_dist_list.append(abs_value)
    min_index = min_dist_list.index(min(min_dist_list))
    x,y = max_list[0][min_index],max_list[1][min_index]
    return x,y,'target'
  else:
    x,y = my_random()
  return x,y,'random'

def scale_matrix(matrix,scale_factor = 2):
  y = matrix.shape[0]
  x = matrix.shape[1]
  big_rows = y * scale_factor
  big_cols = x * scale_factor
  #big_matrix_flat = np.array([x for x in range(big_rows*big_cols)])
  big_matrix = np.zeros((big_rows,big_cols,3),np.uint8)
  #big_matrix = np.reshape(big_matrix_flat,(big_rows,big_cols))
  #print(big_matrix)
  for row in range(y):
      for col in range(x):
          big_matrix[row*scale_factor:row*scale_factor+scale_factor,col*scale_factor:col*scale_factor+scale_factor] = matrix[row][col]
  #print(big_matrix)
  return big_matrix