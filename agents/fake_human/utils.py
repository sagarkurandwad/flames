import numpy as np
from data_pb2 import BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, DOUSED
import math
from math import floor
import random

GO_N = 0
GO_E = 1
GO_S = 2
GO_W = 3
GO_NE = 4
GO_SE = 5
GO_SW = 6
GO_NW = 7

UP = -1
DOWN = 1
RIGHT = +1
LEFT = -1
STAY = 0

EMAT=0
SMAT=1
WMAT=2
NMAT=3
SEMAT=4
SWMAT=5
NWMAT=6
NEMAT=7

"""
direction = {EMAT:[RIGHT,UP],
            SMAT:[RIGHT,DOWN],
            WMAT:[LEFT,DOWN],
            NMAT:[LEFT,UP],
            SEMAT:[RIGHT,UP],
            SWMAT:[RIGHT,DOWN],
            NWMAT:[LEFT,DOWN],
            NEMAT:[LEFT,UP]}
"""

# following is for the copter
direction = {EMAT:[RIGHT,STAY],
            SMAT:[STAY,DOWN],
            WMAT:[LEFT,STAY],
            NMAT:[STAY,UP],
            SEMAT:[RIGHT,DOWN],
            SWMAT:[LEFT,DOWN],
            NWMAT:[LEFT,UP],
            NEMAT:[RIGHT,UP]}


"""
# straight up square
direction = {EMAT:[STAY,UP],
            SMAT:[RIGHT,STAY],
            WMAT:[STAY,DOWN],
            NMAT:[LEFT,STAY],
            SEMAT:[RIGHT,UP],
            SWMAT:[RIGHT,DOWN],
            NWMAT:[LEFT,DOWN],
            NEMAT:[LEFT,UP]}


#direction = [GO_N,GO_E,GO_S,GO_W,GO_NE,GO_SE,GO_SW,GO_NW]
"""

def action_to_dir(action):
  return direction[action]

def get_direction(data):
  
  # these are 5x5s for a 9x9 matrix
  """
  n_mat = np.sum(data[:5,2:-2])
  s_mat = np.sum(data[4:,2:-2])
  w_mat = np.sum(data[2:-2,:5])
  e_mat = np.sum(data[2:-2,4:])
  sw_mat = np.sum(data[4:,0:5])
  nw_mat = np.sum(data[:5,:5])
  ne_mat = np.sum(data[:5,4:])
  se_mat = np.sum(data[4:,4:])
  """
  
  # these are 7x7s for a 15x15 matrix
  n_mat = np.sum(data[:7,4:-4])
  s_mat = np.sum(data[7:,4:-4])
  w_mat = np.sum(data[4:-4,:7])
  e_mat = np.sum(data[4:-4,7:])
  sw_mat = np.sum(data[8:,:7])
  nw_mat = np.sum(data[:7,:7])
  ne_mat = np.sum(data[:7,8:])
  se_mat = np.sum(data[8:,8:])

  # these are 9x9s for a 19x19 matrix
  """
  n_mat = np.sum(data[:9,5:-5])
  s_mat = np.sum(data[9:,5:-5])
  w_mat = np.sum(data[5:-5,:9])
  e_mat = np.sum(data[5:-5,9:])
  sw_mat = np.sum(data[10:,:9])
  nw_mat = np.sum(data[:9,:9])
  ne_mat = np.sum(data[:9,10:])
  se_mat = np.sum(data[10:,10:])
  """

  # these are 3x3s for a 9x9 matrix
  """
  n_mat = np.sum(data[:3,3:6])
  s_mat = np.sum(data[-3:,3:6])
  w_mat = np.sum(data[3:6,:3])
  e_mat = np.sum(data[3:6,-3:])
  sw_mat = np.sum(data[-3:,:3])
  nw_mat = np.sum(data[:3,:3])
  ne_mat = np.sum(data[:3,-3:])
  se_mat = np.sum(data[-3:,-3:])
  """
  powers = [e_mat,s_mat,w_mat,n_mat,se_mat,sw_mat,nw_mat,ne_mat]

  return direction[np.argmax(powers)]

def get_fire_matrices(obs_cells,pos_x,pos_y):
  # embed obs_cells in larger matrix
  pos_x = floor(pos_x)
  pos_y = floor(pos_y)
  pos_x = max(0, min(pos_x, 71))
  pos_y = max(0, min(pos_y, 127))

  #print('TTTTTT2',pos_x,pos_y)

  orig_matrix = np.full((72,128,3),255,np.uint8)
  cell_matrix = np.zeros((72,128),np.uint8)
  for i in range(0,72):
      for j in range(0,128):
          cell_index = j * 72 + i
          #print('TTTTTTT3',cell_index)

          #print('TTTTTTT4',obs_cells[cell_index])
          if obs_cells[cell_index] == ON_FIRE:
            cell_matrix[i][j] = 1
            orig_matrix[i][j] = (0,0,255) # red

  large_cell_matrix = np.zeros((86,142),np.uint8)
  large_cell_matrix[7:-7,7:-7] = cell_matrix
  big_matrix = np.full((86,142,3),255,np.uint8)
  big_matrix[7:-7,7:-7 ] = orig_matrix

  # get 7x7 matrix
  calc_matrix = np.zeros((15,15), np.uint8)
  pos_x += 7
  pos_y += 7
  calc_matrix = large_cell_matrix[pos_x-7:pos_x+8,pos_y-7:pos_y+8]
  fire_count = np.count_nonzero(np.array(calc_matrix.flatten('F')) == 1)


  # get version to display
  little_matrix_rgb = np.full((15,15,3),255,np.uint8)
  little_matrix_rgb = big_matrix[pos_x-7:pos_x+8,pos_y-7:pos_y+8]

  fire_count = np.count_nonzero(np.array(calc_matrix.flatten('F')) == 1)

  return little_matrix_rgb,calc_matrix,fire_count


def my_random(pos_x=36,pos_y=64):
  direction = random.randint(0,3)
  if direction == GO_N:
    pos_y = 0
    pos_x = random.randint(0,71)
  elif direction == GO_E:
    pos_x = 71
    pos_y = random.randint(0,127)
  elif direction == GO_S:
    pos_y = 127
    pos_x = random.randint(0,71)
  elif direction == GO_W:
    pos_x = 0
    pos_y = random.randint(0,127)
  return pos_x,pos_y