import cog_settings

from cogment import Agent, GrpcServer
import random
import math


DECISION_RANGE = 10.0


class Ai_Drone(Agent):
    VERSIONS = {"ai_drone": "1.0.1"}
    actor_class = cog_settings.actor_classes.ai_drone

    def __init__(self, trial, actor):
        print("spinning up agent")
        super().__init__(trial, actor)

    def decide(self, observation):
        action = Ai_Drone.actor_class.action_space()

        my_info = observation.drones[self.id_in_class]

        angle = 2 * math.pi * random.random()
        x = math.cos(angle) * DECISION_RANGE + my_info.position.x
        y = math.sin(angle) * DECISION_RANGE + my_info.position.y

        x = max(0, min(x, 72))
        y = max(0, min(y, 128))

        p = action.path.add()
        p.x = x
        p.y = y

        action.message = "..."
        return action

    def reward(self, reward):
        pass

    def end(self):
        pass


if __name__ == '__main__':
    server = GrpcServer(Ai_Drone, cog_settings)
    server.serve()
