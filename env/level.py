import numpy as np
import time
from opensimplex import OpenSimplex
from data_pb2 import BLANK, TREE, ON_FIRE, UNKNOWN, CHARRED, DOUSED
import random
from math import floor
from scipy.signal import convolve2d

propagation_kernel = np.array([[0.707, 2.0, 0.707],
                               [2.0, 30.0, 2.0],
                               [0.707, 2.0, 0.707]], dtype=np.float)

propagation_kernel = propagation_kernel / np.sum(propagation_kernel)


class LevelState:
    def __init__(self, w=72, h=128, fires=10, seed=None):
        self.w = w
        self.h = h
        self.forest_count = 0
        
        if seed is None:
            seed = time.time()
        random.seed(seed)

        self.truth_matrix = np.zeros(shape=(w, h), dtype=np.int32)
        self.view_matrix = np.zeros(shape=(w, h), dtype=np.int32)
        self.temp_matrix = np.zeros(shape=(w, h), dtype=np.float)
        self.fuel_matrix = np.zeros(shape=(w, h), dtype=np.float)

        self.truth_matrix.fill(BLANK)
        self.view_matrix.fill(UNKNOWN)

        noise = OpenSimplex(seed=random.randint(0, 100000000))
        threshold = 0.1
        scale = 24.0
        for i in range(0, w):
            for j in range(0, h):
                val = noise.noise2d(i / scale, j / scale)
                if val > threshold and random.randint(0, 100) > 10:
                    self.truth_matrix[i][j] = TREE
                    self.fuel_matrix[i][j] = 1.0
                    self.forest_count += 1

        for i in range(fires):
            for i in range(0, 10):
                x = random.randint(0, w-1)
                y = random.randint(0, h-1)
                if self.truth_matrix[x][y] == TREE:
                    self.truth_matrix[x][y] = ON_FIRE
                    self.forest_count -= 1
                    break

        self.initial_forest_count = self.forest_count

    def update_temperature(self):
        w = self.w
        h = self.h
        for i in range(0, w):
            for j in range(0, h):
                if (self.truth_matrix[i][j] == BLANK or
                    self.truth_matrix[i][j] == CHARRED or
                    self.truth_matrix[i][j] == DOUSED):
                    self.temp_matrix[i][j] /= 1.5
                elif self.truth_matrix[i][j] == ON_FIRE:
                    self.fuel_matrix[i][j] -= 0.03
                    if self.fuel_matrix[i][j] <= 0:
                        self.truth_matrix[i][j] = CHARRED
                    else:
                        self.temp_matrix[i][j] = 3.0

        self.temp_matrix = convolve2d(self.temp_matrix, 
                                      propagation_kernel, mode='same')

        for i in range(0, w):
            for j in range(0, h):
                if self.temp_matrix[i][j] >= 1.0 and self.truth_matrix[i][j] == TREE:
                    self.truth_matrix[i][j] = ON_FIRE
                    self.forest_count -= 1

    def reveal(self, locations):
        w = self.w
        h = self.h
        revealed_coords = set()
        for i in range(0, w):
            for j in range(0, h):
                self.view_matrix = np.bitwise_or(self.view_matrix, 32)

        for loc in locations:
            x = loc[0]
            y = loc[1]
            radius = loc[2]
            radius_sq = radius*radius

            for i in range(-radius, radius):
                for j in range(-radius, radius):
                    dst_sq = i*i + j*j
                    if dst_sq < radius_sq:
                        cell_x = floor(x+i)
                        cell_y = floor(y+j)
                        if (cell_x >= 0 and cell_x < w and
                           cell_y >= 0 and cell_y < h):
                            self.view_matrix[cell_x][cell_y] = \
                                self.truth_matrix[cell_x][cell_y]
                            index = cell_y * w + cell_x
                            revealed_coords.add(index)
        return revealed_coords
