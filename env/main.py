import cog_settings
from data_pb2 import Observation, ObservationDelta, Vec2, BLANK, TREE, ON_FIRE
from prometheus_client import start_http_server, Counter, Histogram

from cogment import Environment, GrpcServer

import math
import numpy as np
import time
import os
import traceback
from env.sim import FlamesSim, HELI_SPEED, DRONE_SPEED
from env.viewer import Viewer
from env.utils import Tree_count

import pickle
import sys

ENABLE_VIEWER = bool(os.getenv('ENABLE_VIEWER', False))

created_trials_counter = Counter("trials_created", "Number of trials created")
destroyed_trials_counter = Counter("trials_destroyed", "Number of trials destroyed")
updated_trials_counter = Counter("trials_updates", "Number of trials updates")
update_length = Histogram('trials_update_length', 'Time spent updateing trials')


def lerp(vec_a, vec_b, pct):
    return Vec2(
        x=vec_a.x + (vec_b.x - vec_a.x) * pct,
        y=vec_a.y + (vec_b.y - vec_a.y) * pct
    )


def get_path_distance(path):
    result = 0
    current = path[0]
    for p in path:
        dx = p.x - current.x
        dy = p.y - current.y
        result += math.sqrt(dx*dx + dy*dy)
        current = p
    return result


def get_path_point(path, distance):
    x = path[0].x
    y = path[0].y

    current = path[0]
    for p in path:
        dx = p.x - current.x
        dy = p.y - current.y
        dist = math.sqrt(dx*dx + dy*dy)
        if dist < distance:
            distance -= dist
            current = p
        else:
            pct = distance / dist
            return lerp(current, p, pct)

    return Vec2(x=x, y=y)


class Env(Environment):
    VERSIONS = {"env": "1.0.1"}

    def encode_matrix(self):
        w = self.sim.level.w
        h = self.sim.level.h
        self.observation.remaining_forests = float(self.sim.level.forest_count-self.super_tree_count) / float(self.sim.level.initial_forest_count-self.super_tree_count) 
        self.observation.grid_width = w
        self.observation.grid_height = h

        if len(self.observation.cells) != w * h:
            del self.observation.cells[:]
            self.observation.cells.extend([BLANK] * (w*h))

        self.observation.cells[:] = self.sim.level.view_matrix.flatten('F')

    def encode_object(self, mover, proto):
        proto.position.x = mover.pos.x
        proto.position.y = mover.pos.y
        proto.destination.x = mover.dest.x
        proto.destination.y = mover.dest.y

    def encode_objects(self):
        obs = self.observation
        self.encode_object(self.sim.heli.mover, obs.copter)
        for i, d in enumerate(self.observation.drones):
            self.encode_object(self.sim.drones[i].mover, d)

    def start(self, config):
        try:
            drone_count = len(self.trial.actors.ai_drone)

            created_trials_counter.inc()
            self.sim = FlamesSim(drone_count)

            self.viewer = None
            if ENABLE_VIEWER:
                self.viewer = Viewer(self.sim, self.trial.id)

            self.chopper_path = None
            self.human_is_driving = False
            self.chopper_path_start_time = 0

            obs = Observation()
            obs.drone_names.extend(config.drone_names)
            obs.drone_types.extend(config.drone_types)

            self.observation = obs

            
            for i in range(0, drone_count):
                obs.drones.add()

            self.super_tree_count = 0
            self.encode_objects()
            self.encode_matrix()

            # irv thing for saving initial truth environment
            #self.mycount = 0

            # irv thing
            # with open('newcells.pkl','wb') as f:
            #     pickle.dump(np.array(self.sim.level.truth_matrix.flatten('F')), f)
            #     f.close()

            self.start_tree_count,self.super_tree_count = Tree_count(self.sim.level.truth_matrix)


            if self.viewer is not None:
                self.viewer.refresh_from_sim()

            self.ref_time = time.time()

            self.observation.done = False


            return self.observation
        except Exception as e:
            traceback.print_exc()


    def update(self, actions):
        try:
            updated_trials_counter.inc()
            # Clock advance
            t = time.time()
            dt = t - self.ref_time
            self.ref_time = t

            if dt >= 1.0:
                dt = 1.0

            for id, d in enumerate(self.observation.drones):
                if not self.sim.drones[id].override:
                    d.message = actions.ai_drone[id].message
                    self.sim.drones[id].mover.dest.x = d.destination.x
                    self.sim.drones[id].mover.dest.y = d.destination.y
                else:
                    d.message = "overriden"
            if len(actions.human_plane[0].path) > 0:
                self.human_is_driving = True
                self.chopper_path = actions.human_plane[0].path
                self.chopper_path_start_time = t

            # If there is a second driver
            if len(actions.human_plane) > 1:
                if len(actions.human_plane[1].path) > 0 and (self.chopper_path is None or not self.human_is_driving):
                    self.chopper_path = actions.human_plane[1].path
                    self.chopper_path_start_time = t

            if actions.human_plane[0].HasField('ai_override') and len(actions.human_plane[0].ai_override.path) > 0:
                self.sim.drones[actions.human_plane[0].ai_override_target]  \
                    .set_path_override(actions.human_plane[0].ai_override, t)

            if self.chopper_path:
                total_dst = get_path_distance(self.chopper_path)
                covered = float((t - self.chopper_path_start_time) + 1) * HELI_SPEED * 3
                if covered > 0:
                    if covered >= total_dst:
                        self.sim.heli.mover.dest.x = self.chopper_path[-1].x
                        self.sim.heli.mover.dest.y = self.chopper_path[-1].y
                        self.chopper_path = None
                        self.human_is_driving = False
                    else:
                        dst = get_path_point(self.chopper_path, covered)
                        self.sim.heli.mover.dest.x = dst.x
                        self.sim.heli.mover.dest.y = dst.y

            for d in self.sim.drones:
                if d.override:
                    total_dst = get_path_distance(d.override.path)
                    covered = float((t - d.override_start_time) + 1) * DRONE_SPEED * 3
                    if covered > 0:
                        if covered >= total_dst:
                            dx = d.mover.dest.x - d.mover.pos.x
                            dy = d.mover.dest.y - d.mover.pos.y
                            if dx*dx + dy*dy < 9:
                                d.override = None
                        else:
                            dst = get_path_point(d.override.path, covered)
                            d.mover.dest.x = dst.x
                            d.mover.dest.y = dst.y


            if self.chopper_path:
                total_dst = get_path_distance(self.chopper_path)
                covered = float((t - self.chopper_path_start_time) + 1) * HELI_SPEED * 3
                if covered > 0:
                    if covered >= total_dst:
                        self.chopper_path = None
                    else:
                        dst = get_path_point(self.chopper_path, covered)
                        self.sim.heli.mover.dest.x = dst.x
                        self.sim.heli.mover.dest.y = dst.y

            revealed = self.sim.update(dt)

            for id, act in enumerate(actions.ai_drone):
                if not self.sim.drones[id].override:
                    if len(act.path) > 0:
                        self.sim.drones[id].mover.dest.x = act.path[0].x
                        self.sim.drones[id].mover.dest.y = act.path[0].y

            self.encode_objects()
            self.encode_matrix()
            if self.viewer is not None:
                self.viewer.refresh_from_sim()

                # irv thing
                # if self.mycount == 1:
                #     with open('newcells.pkl','wb') as f:
                #         pickle.dump(np.array(self.sim.level.truth_matrix.flatten('F')), f)
                #         f.close()
                # self.mycount += 1



            t_len = time.time() - t
            update_length.observe(t_len)


            # do the done function here
            fire_count = np.count_nonzero(np.array(self.sim.level.truth_matrix.flatten('F')) == ON_FIRE)  #ON_FIRE)
            if fire_count == 0:
                self.observation.done = True

            delta = ObservationDelta()
            delta.done = self.observation.done
            delta.remaining_forests = self.observation.remaining_forests
            delta.copter.CopyFrom(self.observation.copter)
            delta.drones.extend(self.observation.drones)

            delta.updated_cells_list.extend(revealed)
            delta.new_cell_states.extend([self.observation.cells[x] for x in revealed])

        except Exception as e:
            traceback.print_exc()
            raise

        return delta

    def end(self):
        destroyed_trials_counter.inc()
        if self.viewer is not None:
            self.viewer.destroy()

        print("environment end")


if __name__ == "__main__":
    start_http_server(9999)
    server = GrpcServer(Env, cog_settings)
    server.serve()
