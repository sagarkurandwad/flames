import { Connection } from 'cogment';
import cog_settings from './cog_settings';

import {images, animations} from './resources'
import MouseController from './mouse_controller'
import * as data_pb2 from './data_pb.js';

const STATE_READY = 1;
const STATE_PLAYING = 2;
const STATE_DONE = 3;

const GAME_WIDTH = 72.0;
const GAME_HEIGHT = 128.0;
const DEFAULT_ICON_SIZE = 6.4;


const OBJECT_RANGE_RATIO = 1.2;

const human_class = cog_settings.actor_classes.human_plane;
const drone_class = cog_settings.actor_classes.ai_drone;

const urlParams = new URLSearchParams(window.location.search);
const SPEED = 2.0;
const BACKEND = urlParams.get('backend');
const UID = urlParams.get('uid');
const enable_autoreload = urlParams.get('autoreload') == "true";

const API_URL = "http://" + BACKEND;

const mover_to_object = (mover_info, psim) => {
  const data = new Float32Array(psim.memory.buffer, mover_info);

  return {
    pos: {
      x: data[0],
      y: data[1]
    },
    dest: {
      x: data[2],
      y: data[3]
    },
    vel: {
      x: data[4],
      y: data[5]
    }
  };
}

const object_to_mover = (mover_info, obj, psim) => {
  const data = new Float32Array(psim.memory.buffer, mover_info);

  data[0] = obj.pos.x;
  data[1] = obj.pos.y;
  data[2] = obj.dest.x;
  data[3] = obj.dest.y;
  data[4] = obj.vel.x;
  data[5] = obj.vel.y;
}

class GameObject {
  constructor(name, pos, size, image, dragable) {
    this.name = name;
    this.size = size;
    this.image = image;
    this.dragable = dragable;
    this.pos = pos;
    this.alt_image = null;
    this.drawing_alt = 0;
    this.animation = null;
    this.frame_life = 0;

    this.mover_info = null;
    this.mover_object = null;
    this.pending_action = null;

    this.msg = ""
    this.angle = 0;
  }

  update(game, dt) {
    if(this.mover_info) {
      object_to_mover(this.mover_info, this.mover_object, game.psim);
      game.psim.update_mover(this.mover_info, dt, SPEED);
      this.mover_object = mover_to_object(this.mover_info, game.psim);

      this.pos = this.mover_object.pos;
    }
  }

  draw(ctx, game, dt) {

    const icon_draw_size = this.size * game.scale;
    const pos = game.icon_pos_to_canvas(this.pos, this.size);


    if(this.mover_info) {
      const dst = game.point_to_canvas(this.mover_object.dest);
      const loc = game.point_to_canvas(this.mover_object.pos);

      const dx = this.mover_object.vel.x;
      const dy = this.mover_object.vel.y;
      const dst_sq = dx * dx + dy * dy;

      ctx.strokeStyle = 'blue';
      ctx.lineWidth = 1;
      ctx.beginPath(); 
      ctx.moveTo(loc.x, loc.y);
      ctx.lineTo(dst.x, dst.y);
      ctx.stroke();
      
      if(dst_sq > 0) {
        var angle = Math.atan2(dy, dx) + Math.PI * 0.5;

        if(angle - this.angle > Math.PI) {
          angle -= Math.PI * 2.0;
        }

        if(angle - this.angle < -Math.PI) {
          angle += Math.PI * 2.0;
        }

        this.angle += (angle - this.angle) * 0.1;

        if(this.angle >= Math.PI) {
          this.angle -= Math.PI * 2.0;
        }

        if(this.angle < -Math.PI) {
          this.angle += Math.PI * 2.0;
        }
      } 

      ctx.translate(pos.x + icon_draw_size/2, pos.y + icon_draw_size/2)
      ctx.rotate(this.angle);
      ctx.translate(-icon_draw_size/2, -icon_draw_size/2);
    }


    if(this.animation) {
      this.frame_life -= dt;
      if(this.frame_life <= 0 ) {
        const frames = this.animation.frames; 
        var frame = frames[Math.floor(Math.random()*frames.length)];
        this.frame_life = frame.time;
        this.image = frame.img;
      }
      ctx.drawImage(this.image, 0.0, 0.0, icon_draw_size, icon_draw_size);
    }
    else {
      if(this.drawing_alt >= 2 == 0 || !this.alt_image) {
        ctx.drawImage(this.image, 0.0, 0.0, icon_draw_size, icon_draw_size);
      }
      else {
        ctx.drawImage(this.alt_image, 0, 0, icon_draw_size, icon_draw_size);
      }
  
      this.drawing_alt++ ;
      if(this.drawing_alt >= 4) {
        this.drawing_alt -= 4;
      }
    }

    ctx.resetTransform();

    if(this.name != "") {

    }

    if(this.msg != "" || this.name != "") {

      ctx.fillStyle = this.text_color;
      ctx.strokeStyle = 'white'
      ctx.fillRect(pos.x, pos.y-60, 220, 60)
      ctx.strokeRect(pos.x, pos.y-60, 220, 60)

      ctx.font = '4em Arial';
      ctx.fillStyle = 'white';
      ctx.fillText(this.name, pos.x + 3, pos.y - 5);

      ctx.font = 'bold 2em Arial';
      ctx.fillStyle = 'white';
      ctx.fillText(this.msg, pos.x + 48, pos.y - 10);
    }
  }
};

class Game {
  constructor(canvas, conn_state) {

    this.state = STATE_READY;

    this.ref_time = null;
    this.command_start_loc = [0, 0];

    this.conn_state = conn_state;

    this.cnv = canvas;
    this.ctx = canvas.getContext('2d');

    this.register_listeners();

    this.cog_conn = new Connection(cog_settings, API_URL);
    const cfg = new data_pb2.Config(); 
    cfg.setDumbCount(urlParams.get('dumb'));
    cfg.setSmartCount(urlParams.get('smart'));
    cfg.setSmartestCount(urlParams.get('smartest'));
    cfg.setPilot(urlParams.get('pilot')=='true');

    const webasm_ready = import('./psim.wasm').then(simModule=>{
      this.psim = simModule;
    })


    const connection_ready = this.cog_conn.start_trial(human_class, cfg).then(t => {
      this.trial = t;
    }, reason => {
      this.conn_state.innerHTML = "Connection failed :(. Try again later.";
    });


    Promise.all([ webasm_ready, connection_ready ]).then(()=> {
      this.load_observation(this.trial.observation, true);
      this.start_game();
      this.conn_state.innerHTML = "";
    })


    this.scale = 1.0;
    this.mouse = new MouseController((obj, path)=>{
      if(obj == this.plane) {
        var action = new human_class.action_space();
        var path_points = []
        for(let p of path) {
          var vec = new data_pb2.Vec2();
          vec.setX(p.x);
          vec.setY(p.y);
          path_points.push(vec);
        }
        action.setPathList(path_points);

        this.pending_action = action;
      }
      else {
        var override = new drone_class.action_space();
        
        var path_points = []
        for(let p of path) {
          var vec = new data_pb2.Vec2();
          vec.setX(p.x);
          vec.setY(p.y);
          path_points.push(vec)
        }
        override.setPathList(path_points);

        var action = new human_class.action_space();
        action.setAiOverride(override);
        action.setAiOverrideTarget(this.drones.indexOf(obj));
        this.pending_action = action;
      }
    });

    this.plane_pos = {x: 360.0, y: 640.0};


    this.objects = [];

    this.plane = new GameObject("", {x: 0, y: 0}, DEFAULT_ICON_SIZE * 1.5, images.chopper_1, true);
    this.plane.alt_image = images.chopper_2;
    this.drones = [];


    
    this.resize();

    if (enable_autoreload) {
        const prompt_btn = document.getElementById("prompt");

        prompt_btn.hidden = true;
        this.state = STATE_PLAYING;
        this.request_next_frame();

    }
  }

  load_observation(obs, init) {
    this.obs = obs;
    var score = obs.getRemainingForests();
    score = Math.round(score*1000) / 10.0;
    document.getElementById('score').innerHTML = "Score: " + score + "%";

    if (obs.getDone()) {
      this.trial.end();

      var forest = 0;

      const cells = this.obs.getCellsList();
      for(let c of cells) {
        if( c == data_pb2.CellState.TREE ) {
          forest += 1;
        }
      }
      if(enable_autoreload) {
        window.location.reload();
      }
      this.state = STATE_DONE;

      const prompt_btn = document.getElementById("prompt");
      prompt_btn.innerHTML = "Restart";
      prompt_btn.hidden = false;
      return;
    }

    const drones_src = obs.getDronesList();
    const drone_types = obs.getDroneTypesList();
    const drone_names = obs.getDroneNamesList();

    if(init) {
      this.plane.mover_info = this.psim.js_new_mover();
      this.plane.mover_object = mover_to_object(this.plane.mover_info, this.psim);
      
      this.plane.mover_object.pos.x = obs.getCopter().getPosition().getX();
      this.plane.mover_object.pos.y = obs.getCopter().getPosition().getY();
      this.plane.mover_object.vel.x = 0.0;
      this.plane.mover_object.vel.y = 0.0;

      this.drones = [];
      for(var i = 0 ; i < drones_src.length; ++i) {
        const drone = new GameObject(drone_names[i], {
          x: drones_src[i].getPosition().getX(), 
          y: drones_src[i].getPosition().getY()}, 
          DEFAULT_ICON_SIZE*0.75, images.drone, true);

        if(drone_types[i] == 0) {
          drone.text_color = 'black'
        } else if(drone_types[i] == 1) {
          drone.text_color = 'darkred'
        } else {
          drone.text_color = 'darkgreen'
        }

        drone.mover_info = this.psim.js_new_mover();
        drone.mover_object = mover_to_object(drone.mover_info, this.psim);
        
        drone.mover_object.pos.x = drones_src[i].getPosition().getX();
        drone.mover_object.pos.y = drones_src[i].getPosition().getY();
        drone.mover_object.vel.x = 0.0;
        drone.mover_object.vel.y = 0.0;
        
        this.drones.push(drone);
      }
    }

    this.plane.mover_object.dest.x = obs.getCopter().getDestination().getX();
    this.plane.mover_object.dest.y = obs.getCopter().getDestination().getY();

    for(var i = 0 ; i < drones_src.length; ++i) {
      this.drones[i].msg = drones_src[i].getMessage();
      this.drones[i].mover_object.dest.x = drones_src[i].getDestination().getX();
      this.drones[i].mover_object.dest.y = drones_src[i].getDestination().getY();
      this.drones[i].plane_info = drones_src[i].toObject();
    }


    this.objects = [];
    this.objects = this.objects.concat(this.drones, [this.plane]);

    if(this.state == STATE_PLAYING) {
      this.request_next_frame();
    }
  }

  canvas_to_local(x, y) {
    const cnv = this.cnv;

    const canvas_x = x - cnv.offsetLeft;
    const canvas_y = y - cnv.offsetTop;

    const game_x = canvas_x / this.scale;
    const game_y = canvas_y / this.scale;

    return {
      x: Math.max(1, Math.min(GAME_WIDTH-1, game_x)),
      y: Math.max(1, Math.min(GAME_HEIGHT-1, game_y))
    };
  }

  point_to_canvas(p) {
    return {
      x: (p.x+0.5) * this.scale,
      y: (p.y+0.5) * this.scale
    };
  }

  icon_pos_to_canvas(p, icon_size) {
    return {
      x: (p.x - icon_size/2) * this.scale,
      y: (p.y - icon_size/2)* this.scale
    };
  }

  get_object_at(pos, dragable_only) {
    var closest_obj = null;
    var closest_distance = 1000000000.0;

    for(let o of this.objects) {
      if(dragable_only && !o.dragable) {
        continue;
      }

      const d = {
        x: Math.abs(pos.x - o.pos.x),
        y: Math.abs(pos.y - o.pos.y),
      };

      const ref_dst = (o.size * 0.5 * 1.41421) * OBJECT_RANGE_RATIO;
      const dst_sq = d.x * d.x + d.y * d.y;

      if(dst_sq < closest_distance && dst_sq < ref_dst*ref_dst) {
        closest_obj = o;
        closest_distance = dst_sq;
      }
    }
    return closest_obj;
  }

  register_listeners() {
    const prompt_btn = document.getElementById("prompt");

    prompt_btn.addEventListener("click", (e)=> {
      if(this.state == STATE_READY) {
        prompt_btn.hidden = true;
        this.state = STATE_PLAYING;
        this.request_next_frame();
      }
      else if(this.state == STATE_DONE) {
        window.location.reload();

      }
    });

    // Touch events
    document.body.addEventListener("touchmove", e => {
      if (e.target == this.cnv) {
        var touch = e.touches[0];
        this.mouse.command_update(
          this.canvas_to_local(touch.clientX, touch.clientY)
        );
        e.preventDefault();
        e.stopImmediatePropagation();
      }
    }, { passive: false });


    // Mouse events
    this.cnv.addEventListener("pointerdown", e => {
      this.cnv.setPointerCapture(e.pointerId);
      const pos = this.canvas_to_local(e.clientX, e.clientY);
      const obj = this.get_object_at(pos, true);
      if(obj) { 
        this.mouse.command_start(
          pos, false, obj
        );
      }
    }, false);
    
    this.cnv.addEventListener("pointerup", e => {
      this.cnv.releasePointerCapture(e.pointerId);
      this.mouse.command_end();
    }, false);

    this.cnv.addEventListener("mousemove", e => {
      this.mouse.command_update(
        this.canvas_to_local(e.clientX, e.clientY)
      );
    }, false);

    // Misc events
    window.addEventListener('resize', ()=>{this.resize();});
  }

  request_next_frame() {
    var action = this.pending_action;
    this.pending_action = null;
    if(!action) {
      action = new human_class.action_space();
    }

    this.trial.do_action(action).then( obs=>{
      this.load_observation(obs, false);
    });
  }

  start_game() {
    console.log("starting game");
//    this.timer = window.setInterval(()=>{this.update();}, 1000);
    window.requestAnimationFrame((t)=>{this.render(t);});
  }

  end_game() {
    this.trial.end();
  }

  // resizes the canvas to whatever largest 9/16 space we can fit it in.
  resize() {
    var w = window.innerWidth;
    var h = window.innerHeight;

    var cnv_w = w;
    var cnv_h = w * 16.0 / 9.0;

    if(cnv_h > h) {
      cnv_h = h;
      cnv_w = h * 9.0 / 16.0;
    }

    this.cnv.width = cnv_w;
    this.cnv.height = cnv_h;

    this.scale = cnv_w / GAME_WIDTH;
  }

  render(timestamp) {

    if(!this.ref_time) {
      this.ref_time = timestamp;
    }
    const dt = (timestamp - this.ref_time) / 1000.0;
    this.ref_time = timestamp;

    // Draw objects
    var dt_accum = dt;
    while(dt_accum > 0.016) {
      dt_accum -= 0.016;
      for( let o of this.objects) {
        o.update(this, 0.016);
      }
    }


    this.ref_time = timestamp;
    const ctx = this.ctx;
    const cnv = this.cnv;

    // reset
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    
    // clear
    ctx.clearRect(0, 0, cnv.width, cnv.height);


    // Draw background
    const grid_width = this.obs.getGridWidth();
    const grid_height = this.obs.getGridHeight();
    const cell_width = GAME_WIDTH / grid_width * this.scale;
    const cell_height = GAME_HEIGHT / grid_height * this.scale;

    const cells = this.obs.getCellsList();
    for(var i = 0 ; i < grid_width; ++i) {
      for(var j = 0 ; j < grid_height; ++j) {
        const index = j * grid_width + i;
        if(cells[index] == data_pb2.CellState.BLANK ) {
          ctx.fillStyle = '#ffeca8';
        } else if (cells[index] == data_pb2.CellState.ON_FIRE) {
          ctx.fillStyle = '#FF0000';
        } else if (cells[index] == data_pb2.CellState.TREE) {
          ctx.fillStyle = '#00FF00';
        } else if (cells[index] == data_pb2.CellState.UNKNOWN) {
          ctx.fillStyle = '#555555';
        }
        else if (cells[index] == data_pb2.CellState.CHARRED) {
          ctx.fillStyle = '#c7793a';
        }
        else if (cells[index] == data_pb2.CellState.DOUSED) {
          ctx.fillStyle = '#daa520';
        }



        if(cells[index] == data_pb2.CellState.BLANK_OLD ) {
          ctx.fillStyle = '#8a7f5b';
        } else if (cells[index] == data_pb2.CellState.ON_FIRE_OLD) {
          ctx.fillStyle = '#880000';
        } else if (cells[index] == data_pb2.CellState.TREE_OLD) {
          ctx.fillStyle = '#008800';
        } else if (cells[index] == data_pb2.CellState.UNKNOWN_OLD) {
          ctx.fillStyle = '#555555';
        }
        else if (cells[index] == data_pb2.CellState.CHARRED_OLD) {
          ctx.fillStyle = '#59381d';
        }
        else if (cells[index] == data_pb2.CellState.DOUSED_OLD) {
          ctx.fillStyle = '#6d5210';
        }
        ctx.fillRect(cell_width*i-0.5, cell_height*j-0.5, cell_width+1.0, cell_height+1.0);
      }
    }



    // Draw user input path if applicable
    const path = this.mouse.path;
    if(path.length > 0 ) {
      ctx.save();
      ctx.beginPath();
      var p = this.point_to_canvas(path[0]);
      ctx.setLineDash([5, 10]);/*dashes are 5px and spaces are 3px*/
      ctx.lineWidth = 1;
      ctx.strokeStyle = "#CC0000";
      ctx.moveTo(p.x, p.y);
      for(var i = 1; i < path.length; i++) {
        p = this.point_to_canvas(path[i]);
        ctx.lineTo(p.x, p.y);
      }
      ctx.stroke();
      ctx.restore();
    }

    // Draw objects
    for( let o of this.objects) {
      o.draw(ctx, this, dt);
    }


    
    window.requestAnimationFrame((t)=>{this.render(t);});
  }
}

export default Game;