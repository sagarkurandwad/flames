export function apply_delta(observation, delta) {
  observation.setCopter(delta.getCopter());
  observation.setDronesList(delta.getDronesList());
  observation.setDone(delta.getDone());
  observation.setRemainingForests(delta.getRemainingForests());

  var cells = observation.getCellsList();

  for(var i = 0 ; i < cells.length; ++i) {
    if(cells[i] < 32) cells[i] += 32;
  }

  var updates = delta.getUpdatedCellsListList();
  var vals = delta.getNewCellStatesList();

  for(var i = 0 ; i < updates.length; ++i) {
    cells[updates[i]] = vals[i];
  }
  
  return observation;
}