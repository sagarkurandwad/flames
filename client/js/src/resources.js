
export const images = {
  plane: new Image(),
  chopper_1: new Image(),
  chopper_2: new Image(),
  fire_1: new Image(),
  fire_2: new Image(),
  fire_3: new Image(),
  drone: new Image(),
  fire: new Image(),
  pointer: new Image()
}


export const animations = {
  fire: {
    randomize: true,
    frames: [
    {
      img:images.fire_1,
      time: 0.1
    },
    {
      img:images.fire_2,
      time: 0.1
    },
    {
      img:images.fire_3,
      time: 0.1
    }]
  },
  chopper: {
    randomize: false,
    frames: [
    {
      img:images.chopper_1,
      time: 0.03
    },
    {
      img:images.chopper_2,
      time: 0.03
    }]
  }
}

images.plane.src = '/img/plane.png';
images.drone.src = '/img/drone.png';
images.fire.src = '/img/fire.png';
images.chopper_1.src = '/img/chopper_1.png';
images.chopper_2.src = '/img/chopper_2.png';
images.fire_1.src = '/img/fire_1.png';
images.fire_2.src = '/img/fire_2.png';
images.fire_3.src = '/img/fire_3.png';
images.pointer.src = '/img/pointer.png';