#AI-r flames demo.

**This is not a public project at the moment**

## Building

The `build.sh` script contains a shorthand script to build the project on unix-like platforms. It unconditionally builds all parts of the project at once, so it might be a bit overkill, but it's still fast enough to not be a bother at the moment.

## Running

The following command will launch everything locally.

```
docker-compose up nginx envoy
```

You can access the game at `http://localhost` or `http://your-ip-here` from your favorite browser. 