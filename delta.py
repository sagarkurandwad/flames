
def apply_delta(observation, delta):


    for i in range(len(observation.cells)):
        if observation.cells[i] < 32:
            observation.cells[i] += 32

    count = len(delta.updated_cells_list)
    for i in range(0, count):
        observation.cells[delta.updated_cells_list[i]] = \
          delta.new_cell_states[i]

    observation.copter.CopyFrom(delta.copter)
    del observation.drones[:]
    observation.drones.extend(delta.drones)

    observation.done = delta.done
    observation.remaining_forests = delta.remaining_forests
    
    return observation
